$(document).on('vclick', '#view-complaints-link', function(){  
	
	$.get('http://3dbb46ca.ngrok.com/complaint/user/divya',function(complaints){
		$("#myIssuesContent #issue-list").empty();
		$.each(complaints,function(i,complaint){
			addToComplaintsView("myIssuesContent",complaint);
		});
		$.mobile.changePage("#viewComplaintsPage", {transition: "slide", changeHash: false});
	});

	$.get('http://3dbb46ca.ngrok.com/complaint/area/Perungudi',function(complaints){
		$("#areaIssuesContent #issue-list").empty();
		$.each(complaints,function(i,complaint){
			addToComplaintsView("areaIssuesContent",complaint);
		});
	});

	$.get('http://3dbb46ca.ngrok.com/complaint/filter/Drainage',function(complaints){
		$("#filtersContent #issue-list").empty();
		$.each(complaints,function(i,complaint){
			addToComplaintsView("filtersContent",complaint);
		});
	});

});

$(document).on('vclick','#my-municipality',function(){
	createMap();
	$.mobile.changePage("#myMunicipalityViewPage", {transition: "slide", changeHash: false});
});

$(document).on('vclick','#announcement',function(){
	$.mobile.changePage("#announcementViewPage", {transition: "slide", changeHash: false});
});

$(document).on('vclick','#user-profile',function(){
	$.mobile.changePage("#userProfileViewPage", {transition: "slide", changeHash: false});
});

$(document).on('vclick', '#issue-list li a', function(){  
	var complaintId = $(this).attr('id');
	// Get the complaints raised by current user
	$.get('http://3dbb46ca.ngrok.com/complaint/byId/'+complaintId,function(complaint){
		$('.issueDetails').empty();
		var imageUrl = 'https://s3-ap-southeast-1.amazonaws.com/mycitycomplaintimages/' + complaint.image;
		var detailText = "<h2>Complaint Details</h2>"+
				"<p>Title : "+complaint.title+"</p>"+
				"<p>Type : "+complaint.complaintType+"</p>"+
				"<p><img src='" + imageUrl + "' style='width:420px;height:300px;margin:2%;'/></p>"+
				"<p>Status : "+complaint.status+"</p>"+
				"<p>Desc : "+complaint.desc+"</p>"+
				"<div id='googleMap' style='width:420px;height:300px;margin:2%;'>" + "</div>" +
				"<p>"+complaint.street+","+complaint.location+","+complaint.area+"</p>";
		$('.issueDetails').append(detailText);
		createMap();
		$.mobile.changePage("#issueDetailViewPage", {transition: "slide", changeHash: false});
	});
	
});

function addToComplaintsView(parentId,complaint){
	var id = complaint._id;
	var title = complaint.title;
	var complaintDOM = '<li><a id="'+id+'">'+title+'</a></li>'
	$('#'+parentId+' #issue-list').append(complaintDOM);
}

var s3Uploader = (function () {
    var s3URI = encodeURI("https://mycitycomplaintimages.s3.amazonaws.com/"),
        policyBase64 = "eyJleHBpcmF0aW9uIjoiMjAyMC0xMi0zMVQxMjowMDowMC4wMDBaIiwiY29uZGl0aW9ucyI6W3siYnVja2V0IjoibXljaXR5Y29tcGxhaW50aW1hZ2VzIn0sWyJzdGFydHMtd2l0aCIsIiRrZXkiLCIiXSx7ImFjbCI6InB1YmxpYy1yZWFkIn0sWyJzdGFydHMtd2l0aCIsIiRDb250ZW50LVR5cGUiLCIiXSxbImNvbnRlbnQtbGVuZ3RoLXJhbmdlIiwwLDUyNDI4ODAwMF1dfQ==",
        signature = "n44J03NgAmXiuf+rCBI/jQahT6E=",
        awsKey = 'AKIAJNKFSPWBA7SXD4KA',
        acl = "public-read";

    function upload(imageURI, fileName) {
	    var deferred = $.Deferred();
		var ft = new FileTransfer();
	    var options = new FileUploadOptions();
		
        options.fileKey = "file";
        options.fileName = fileName;
        options.mimeType = "image/jpeg";
        options.chunkedMode = false;
        options.params = {
            "key": fileName,
            "AWSAccessKeyId": awsKey,
            "acl": acl,
            "policy": policyBase64,
            "signature": signature,
            "Content-Type": "image/jpeg"
        };
		ft.upload(imageURI, s3URI,
            function (e) {
		        deferred.resolve(e);
            },
            function (e) {
				deferred.reject(e);
            }, options);

        return deferred.promise();

    }

    return {
        upload: upload
    }

}());

//Take a picture using the camera
function takePicture(e) {
	var options = {
        quality: 45,
        targetWidth: 1000,
    	targetHeight: 1000,
		destinationType: Camera.DestinationType.FILE_URI,
		encodingType: Camera.EncodingType.JPEG,
		sourceType: Camera.PictureSourceType.CAMERA,
	};

	navigator.camera.getPicture(function (imageURI) {
		// Show image in mobile ui
		$('#picture').attr('src', imageURI);
		fileName = "" + (new Date()).getTime() + ".jpg"; // consider a more reliable way to generate unique ids
		$('#filename').html(fileName);
		s3Uploader.upload(imageURI, fileName)
			.done(function () {
			})
			.fail(function () {
			});
		},
		function (message) {
			alert('error');
			// We typically get here because the use canceled the photo operation. Fail silently.
		}, options);

	return false;
};

$(document).on('vclick', '#getPic',takePicture);
	
$(document).on('vclick', '#getLoc', function(){  
	   alert("get Location test");
	   if(navigator.geolocation){
	   alert("yes");
	   }
	   else{
	   alert("no");
	   }
	   navigator.geolocation.getCurrentPosition(onLocationSuccess, onLocationError);
	   alert("end location");
});
		
function onLocationSuccess(position) {
    alert('Latitude: '          + position.coords.latitude          + '\n' +
          'Longitude: '         + position.coords.longitude         + '\n' +
          'Altitude: '          + position.coords.altitude          + '\n' +
          'Accuracy: '          + position.coords.accuracy          + '\n' +
          'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
          'Heading: '           + position.coords.heading           + '\n' +
          'Speed: '             + position.coords.speed             + '\n' +
          'Timestamp: '         + position.timestamp                + '\n');
}

// onError Callback receives a PositionError object
function onLocationError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

$(document).on('vclick', "#sendComp", function(){
	var compTitle = $('#compliantTitle').val();
	var compDesc = $('#compliantDesc').val();
	var compCategory = $('#category :selected').text();
	var data = {"area":"Perungudi", "location" : "Kamaraj Nagar",  
		"street" : "Kamaraj Nagar 1st Cross Street", "title" : compTitle, "desc" : compDesc,  "user" : "divya",   
		"complaintType" : compCategory, "image" : fileName};
	 $.ajax({
               url: 'http://3dbb46ca.ngrok.com/complaint/create',
               async: true,
               type: 'POST',
			   data: JSON.stringify(data),
               /*contents:{
                       area : 'Choolaimedu',
                       location : 'East Namachivayapuram',
                       street : 'East Namachivayapuram 1st Cross Street',
                       title : compTitle,
                       desc : compDesc,
                       user : 'divya',
                       complaintType : compCategory,
					   image : fileName
               
               },*/
			   contentType:'application/json',
               success: function (resp) {
				
			    $.mobile.changePage("#complaintSuccess", {transition: "slide", changeHash: false});
               },
               error: function (error) {
                      
			$.mobile.changePage("#complaintSuccess", {transition: "slide", changeHash: false});
               }
   });
});


function createMap()
{
	var myLatlng = new google.maps.LatLng(12.9707804,80.2441985);
	var mapCanvas = document.getElementById('googleMap');
	var mapOptions = {
    	center: myLatlng,
      	zoom: 12,
      	mapTypeId: google.maps.MapTypeId.ROADMAP
    }
	var map = new google.maps.Map(mapCanvas, mapOptions);
	var marker = new google.maps.Marker({
    	position: myLatlng,
      	map: map
	});
}
